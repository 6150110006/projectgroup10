package com.example.phpandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Register extends AppCompatActivity {

    public static String BaseUrl = "https://itlearningcenters.com/android/project0810/regist_shop.php";
    EditText email,name,password,address;
    Button register;
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        email=(EditText)findViewById(R.id.email);
        name=(EditText)findViewById(R.id.name);
        name=(EditText)findViewById(R.id.address);
        password=(EditText)findViewById(R.id.password);
        register=(Button) findViewById(R.id.register);

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String user_mail = email.getText().toString();
                String user_name = name.getText().toString();
                String user_password = password.getText().toString();

                Retrofit retrofit = new Retrofit.Builder().baseUrl("http://172.20.217.218/php_android/")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                RequestInterface reques = retrofit.create(RequestInterface.class);
                Call<JsonResponse> call = reques.create(user_name,user_password,user_mail);
                call.enqueue(new Callback<JsonResponse>() {
                    @Override
                    public void onResponse(Call<JsonResponse> call, Response<JsonResponse> response) {
                        if (response.code()==200) {
                            JsonResponse response1 = response.body();
                            Toast.makeText(getApplicationContext(),response1.getResponse().toString(),Toast.LENGTH_SHORT).show();

                        }
                        else {
                            Toast.makeText(getApplicationContext(),String.valueOf(response.code()),Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonResponse> call, Throwable t) {
                        Toast.makeText(getApplicationContext(),"Error",Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });


    }
}